class Character {
    constructor(id, name, image, description, level, vigor, mind, endurance, strength, dexterity, inteligence,) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.description = description;
    this.level = level;
    this.vigor = vigor;
    this.mind = mind;
    this.endurance = endurance;
    this.strength = strength;
    this.dexterity = dexterity;
    this.inteligence = inteligence;
    }
    
    getCharacterId() {
        return this.id;
    }

    getCharacterName() {
        return this.name;
    }

    getCharacterImg() {
        return this.image;
    }

    getCharacterDescription() {
        return this.description;
    }

    getCharacterLevel() {
        return this.level;
    }

    getCharacterVigor() {
        return this.vigor;
    }

    getCharacterMind() {
        return this.mind;
    }

    getCharacterEndurance() {
        return this.endurance;
    }

    getCharacterStrength() {
        return this.strength;
    }

    getCharacterDexterity() {
        return this.dexterity;
    }

    getCharacterInteligence() {
        return this.inteligence;
    }


    setCharacterAlias(CharacterAlias) {
    this.name = CharacterAlias;
    }
}

class CharacterDetailClass extends Character {
    constructor(id, name, image, description, level, vigor, mind, endurance, strength, dexterity, inteligence, faith, arcane) {
    super(id, name, image, description, level, vigor, mind, endurance, strength, dexterity, inteligence);
    this.faith = faith;
    this.arcane = arcane;
    }

    getCharacterFaith() {
        return this.faith;
    }

    getCharacterArcane() {
        return this.arcane;
    }
};


export { Character, CharacterDetailClass };
