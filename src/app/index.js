import { classListEldenRing, next, previous, toggleBtn } from './view/view-eldenRing-class-list';
import { classEldenDetail } from './view/view-detail-class';


import './styles/styles.scss';
import 'bootstrap';

// añadimos los listenes a nuestros botones.
const addListeners = () => {
  document.getElementById('btnEldenRing').addEventListener('click', classListEldenRing);
  document.getElementById("btnEldenRing").addEventListener("click", toggleBtn);
  document.getElementById("eldenFinder").addEventListener("click", classEldenDetail);
  document.getElementById("previous").addEventListener("click", previous);
  document.getElementById("next").addEventListener("click", next);
  console.log('addListeners');
};

window.onload = function () {
  addListeners();
};
