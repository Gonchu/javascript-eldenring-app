import { getClassDetail } from '../api/call-to-api-detail';
import { CharacterDetailClass } from '../models/personaje';

const classEldenDetail = async () => {
    const eldenRingFind = document.getElementById('eldenRingFind');
    const eldenName = document.getElementById('eldenName').value;
    const character = await getClassDetail(eldenName);
    const eldenDetail = new CharacterDetailClass(
        character[0].id,
        character[0].name,
        character[0].image,
        character[0].description,
        character[0].stats.level,
        character[0].stats.vigor,
        character[0].stats.mind,
        character[0].stats.endurance,
        character[0].stats.strength,
        character[0].stats.dexterity,
        character[0].stats.inteligence,
        character[0].stats.faith,
        character[0].stats.arcane
    );


    const eldenHTMLString =
    `<li class="flex-item">
        <h2 class="flex-item-title">${eldenDetail.getCharacterName()}</h2>
        <img class="flex-item-image" src="${eldenDetail.getCharacterImg()}"/>
        <p class="flex-item-title">ID: ${eldenDetail.getCharacterId()}</p>
        <p class="flex-item-paragraph">${eldenDetail.getCharacterDescription()}</p>
        <p class="flex-item-paragraph">LEVEL: ${eldenDetail.getCharacterLevel()} // VIGOR: ${eldenDetail.getCharacterVigor()}</p>
        <p class="flex-item-paragraph">MIND: ${eldenDetail.getCharacterMind()} // ENDURANCE: ${eldenDetail.getCharacterEndurance()}</p>
        <p class="flex-item-paragraph">STRENGTH: ${eldenDetail.getCharacterStrength()} // DEXTERITY: ${eldenDetail.getCharacterDexterity()}</p>
        <p class="flex-item-paragraph">INTELIGENCE: ${eldenDetail.getCharacterInteligence()} // FAITH: ${eldenDetail.getCharacterFaith()}</p>
        <p class="flex-item-paragraph">ARCANE: ${eldenDetail.getCharacterArcane()}</p>
    </li>`;
    if (eldenRingFind.classList.contains('hide')){
        eldenRingFind.classList.remove('hide');
    };
    eldenRingFind.innerHTML = eldenHTMLString;
    let eldenRing = document.getElementById("eldenRing");
    if (!eldenRing.classList.contains('hide')){
        eldenRing.classList.add('hide');
    };
};

export { classEldenDetail };


