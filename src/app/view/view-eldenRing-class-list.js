import { getEldenRingPage } from '../api/call-to-api-page';
import { getEldenRingClassList } from '../api/call-to-api-classList';

let pagina=0;
let maxPaginas=0;
let clasesPorPagina = 2;

function listar(classListPage) {
    const eldenRing = document.getElementById('eldenRing');
    eldenRing.innerHTML="";
    classListPage.then((classList) => {
        classList.forEach((clase) => {
            const div = document.createElement('div');
            div.classList.add('col-6');
            const h3 = document.createElement('h3');
            h3.setAttribute('class', 'title');
            h3.innerText = clase.name;
            const img = document.createElement('img');
            img.setAttribute('class', 'img-item');
            img.setAttribute('src', clase.image);
            const p = document.createElement('p');
            p.innerHTML = `${clase.description}`;
            eldenRing.appendChild(div);
            div.appendChild(h3);
            div.appendChild(img);
            div.appendChild(p);
        });
        
    });
};

// inicializo la pagina 0, llamo a las primeras clases y saco el maximo de paginas.
const classListEldenRing = () => {
    pagina = 0;
    let arrayClases = getEldenRingPage(pagina, clasesPorPagina);//llamada a la api.
    listar(arrayClases);//muestro las clases.

    // saco el número máximo de páginas que hay.
    let arrayTodasClases = getEldenRingClassList();
    //maxPaginas = arrayTodasClases.length / clasesPorPagina;
    maxPaginas=7;

    // Elimino el listener de esta llamada para que solo se ejecute la primera vez.
    document.getElementById('btnEldenRing').removeEventListener('click', classListEldenRing);
};

const toggleBtn = function() {
    console.log('toggleBtn');
    let eldenRing = document.getElementById("eldenRing");
    if (eldenRing.classList.contains('hide')){
        eldenRing.classList.remove('hide');
    } else {
        eldenRing.classList.add('hide');
    }; 
    let eldenRingFind = document.getElementById("eldenRingFind");
    if (!eldenRingFind.classList.contains('hide')){
        eldenRingFind.classList.add('hide');
    }
};

    const next = function() {
        if(pagina < maxPaginas ) {
            pagina++;
            let arrayClases = getEldenRingPage(pagina, clasesPorPagina);
            listar(arrayClases);
        }
    }

    
    const previous = function() {
        if(pagina > 0) {
            pagina--;
            let arrayClases = getEldenRingPage(pagina, clasesPorPagina);
            listar(arrayClases);
        }

    }

export { classListEldenRing, toggleBtn, next, previous };